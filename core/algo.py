import sqlite3
import math
DB_PATH = "data.db"

class Generator:
    def __init__(self, type, name, price, power, serviceLife, fuelConsumption = 0):
        self.type = type
        self.name = name
        self.price = price
        self.power = power
        self.serviceLife = serviceLife
        self.fuelConsumption = fuelConsumption
        self.amortizationByHour = self.price / (self.serviceLife * 365 * 24)

    def getEfficiency(self, gasPrice = 0, diselPrice = 0):
        if self.type == "gas":
            priceOfFuel = gasPrice
        elif self.type == "disel":
            priceOfFuel = diselPrice
        else:
            priceOfFuel = 0
        self.Efficiency = ((self.price / (self.serviceLife * 365 * 24)) + self.fuelConsumption * priceOfFuel) / self.power
        return self.Efficiency

class Database:
    def __init__(self, path: str, types: list):
        def _remakeAcData(badData : list) -> list:
            newData = []
            for i in badData:
                newData.append([i[1], i[2], i[3], i[4]])
            return newData

        def _remakeData(badData: list) -> list: 
            # переводит всю информацию по генераторам из БД в лист объетов класса Generator
            # badData - данные из БД (cursor.fetchall())
            types = self.types
            goodData = []
            for i in types:
                goodData.append([])
            for generator in badData:
                id = generator[0]
                type = generator[1]
                name = generator[2]
                price = generator[3]
                power = generator[4]
                serviceLife = generator[5]
                fuelConsumption = generator[6]
                if type in types:
                    goodData[types.index(type)].append(Generator(type, name, price, power, serviceLife, fuelConsumption))
            return goodData
        self.path = path
        self.types = types
        self.GenData = []
        self.AccumData = []
        self.InvertersData = []
        self.SubjectsData = []
        with sqlite3.connect(path) as data:
            cursor = data.cursor()
            cursor.execute("""CREATE TABLE IF NOT EXISTS generators(
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    type VARCHAR(10),
                    name VARCHAR(256), 
                    price INTEGER, 
                    power REAL, 
                    serviceLife INTEGER,
                    fuelConsumption REAL
            )""")
            cursor.execute("""CREATE TABLE IF NOT EXISTS subjects(
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name VARCHAR(256),
                    sunSpeed REAL, 
                    windSpeed REAL
            )""")
            cursor.execute("""CREATE TABLE IF NOT EXISTS accumulators(
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name VARCHAR(256),
                    volume REAL, 
                    price INTEGER, 
                    serviceLife INTEGER
            )""")
            cursor.execute("""CREATE TABLE IF NOT EXISTS inverters(
                    id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name VARCHAR(256),
                    power REAL, 
                    price INTEGER, 
                    serviceLife INTEGER
            )""")
            cursor.execute("SELECT * FROM generators")
            self.GenData = _remakeData(cursor.fetchall())

            cursor.execute("SELECT * FROM accumulators")
            self.AccumData = _remakeAcData(cursor.fetchall())

            cursor.execute("SELECT * FROM inverters")
            self.InvertersData = _remakeAcData(cursor.fetchall()) #AcData аналогичен, поэтому зачем дублировать код
        
    def getData(self, type = "all") -> list[Generator]:
        # возвращает всю инфу по генераторам из БД, опционально возвращает генераторы только определенного типа
        if type == "all":
            return self.GenData
        else:
            if type in self.types:
                return self.GenData[self.types.index(type)]
            else:
                print(f"WARNING! getData has bad type parametr: {type}")
                print(f"types is {self.types}")
                return None

# ["sun", "wind", "disel", "gas"]
def sumPower(gens, i1, i2, i3, i4):
    s = 0
    x = 0
    for i in gens:
        x += 1
        s += i.power * eval(f"i{x}")
    return s

def sumPrice(gens, i1, i2, i3, i4, pg, pd):
    p = 0
    x = 0
    for i in gens:
        x += 1
        p += (i.amortizationByHour + (i.type == "gas") * pg * i.fuelConsumption + (i.type == "disel") * pd * i.fuelConsumption) * eval(f"i{x}")
        
    return p


def getSelectedGenerators(sunInsolation : float, windSpeed : float, diselPrice : float, gasPrice : float, types : list):
    global DB_PATH
    db = Database(DB_PATH, types) # шаг 1.1 сделан
    data = db.GenData
    selectedGenerators = []
    for genByType in data: #1.2 - 1.3
        if len(genByType) > 0:
            min_ = genByType[0].getEfficiency(gasPrice, diselPrice)
        else:
            min_ = 9999999999999999
        for gen in genByType:
            min_ = min(min_, gen.getEfficiency(gasPrice, diselPrice))
        for gen in genByType:
            if gen.Efficiency == min_:
                selectedGenerators.append(gen)
                break
    for gen in selectedGenerators: #1.4
        match gen.type:
            case "sun":
                gen.power *= 0.85 * sunInsolation
                gen.getEfficiency
            case "wind":
                gen.power *= (windSpeed / 11)
                gen.getEfficiency
    return selectedGenerators

def addAccumulators(NEED_ENERGY : float, accumData : list, reservedHours : int = 12) -> float:
    needVolume = (NEED_ENERGY / (30*24)) * reservedHours
    min_price = math.ceil(needVolume / accumData[0][1]) * accumData[0][2]
    min_price_index = 0
    for i in range(len(accumData)):
        if (min_price > math.ceil(needVolume / accumData[i][1]) * accumData[i][2]):
            min_price = math.ceil(needVolume / accumData[i][1]) * accumData[i][2]
            min_price_index = i
    return [accumData[min_price_index][0], math.ceil(needVolume / accumData[i][1]), math.ceil(needVolume / accumData[i][1]) * accumData[i][2] / (accumData[i][3] * 12)]
    
def addInverters(needPower : float, invertersData : list) -> float:
    min_price = math.ceil(needPower / invertersData[0][1]) * invertersData[0][2]
    min_price_index = 0
    for i in range(len(invertersData)):
        if (min_price > math.ceil(needPower / invertersData[i][1]) * invertersData[i][2]):
            min_price = math.ceil(needPower / invertersData[i][1]) * invertersData[i][2]
            min_price_index = i
    return [invertersData[min_price_index][0], math.ceil(needPower / invertersData[i][1]), (math.ceil(needPower / invertersData[i][1]) * invertersData[i][2]) / (invertersData[i][3] * 12)]

def algo(sunInsolation: float, windSpeed : float, diselPrice : float, gasPrice : float, NEED_ENERGY : float, electricityPrice : float, types : list):
    #1 start
    selectedGenerators = getSelectedGenerators(sunInsolation, windSpeed, diselPrice, gasPrice, types)
    #1 end

    #2 start
    NeedEnergyByHour = NEED_ENERGY / (61 * 12)
    for i in selectedGenerators:
        if i.power == 0:
            i.power = 0.0000000001
    match len(selectedGenerators):
        case 0:
            print("Error! Selected Generators is Null")
            return "Error 1"
        case 1:
            end1 = int((NeedEnergyByHour / selectedGenerators[0].power) + 1)
            end2 = 0
            end3 = 0
            end4 = 0
        case 2:
            end1 = int((NeedEnergyByHour / selectedGenerators[0].power) + 1)
            end2 = int((NeedEnergyByHour / selectedGenerators[1].power) + 1)
            end3 = 0
            end4 = 0
        case 3:
            end1 = int((NeedEnergyByHour / selectedGenerators[0].power) + 1)
            end2 = int((NeedEnergyByHour / selectedGenerators[1].power) + 1)
            end3 = int((NeedEnergyByHour / selectedGenerators[2].power) + 1)
            end4 = 0
        case 4:
            end1 = int((NeedEnergyByHour / selectedGenerators[0].power) + 1)
            end2 = int((NeedEnergyByHour / selectedGenerators[1].power) + 1)
            end3 = int((NeedEnergyByHour / selectedGenerators[2].power) + 1)
            end4 = int((NeedEnergyByHour / selectedGenerators[3].power) + 1)
    
    min_ = 999999999999999999999999999991
    equipQuantity = []
    for i1 in range(0, end1 + 1, max(1, int(end1/10))):
        for i2 in range(0, end2 + 1, max(1, int(end2/10))):
            for i3 in range(0, end3 + 1, max(1, int(end3/10))):
                for i4 in range(0, end4 + 1, max(1, int(end4/10))):
                    summaryPower = sumPower(selectedGenerators, i1, i2, i3, i4)
                    summaryPrice = sumPrice(selectedGenerators, i1, i2, i3, i4, gasPrice, diselPrice)
                    if (summaryPower < NeedEnergyByHour):
                        deltaEnergy = NeedEnergyByHour - summaryPower
                        summaryPrice += deltaEnergy * electricityPrice
                    if (summaryPrice < min_):
                        min_ = summaryPrice
                        equipQuantity = [i1, i2, i3, i4]

    resultPrice = int(min_ * 12 * 61)
    gens = []
    for i in range(len(equipQuantity)):
        if equipQuantity[i] != 0:
            gens.append([selectedGenerators[i].name, equipQuantity[i]])
    print(resultPrice, gens)

    for i in gens:
        if ('Solar' in i[0]):
            db = Database(DB_PATH, types) # шаг 1.1 сделан
            data = db.GenData
            for j in data[0]:
                if (j.name == i[0]):
                    need_power_for_inverters = j.power * i[1]

            inverters = addInverters(need_power_for_inverters, db.InvertersData)
            invertName = inverters[0]
            invertQuantity = inverters[1]
            invertersPrice = inverters[2]
            gens.append([invertName, invertQuantity])
            resultPrice += math.ceil(invertersPrice*100)/100
    
    if (len(gens) != 0):
        db = Database(DB_PATH, types)
        accums = addAccumulators(NEED_ENERGY, db.AccumData, 12)
        resultPrice += math.ceil(accums[2]*100)/100
        gens.append([accums[0], accums[1]])    

    return math.ceil(resultPrice * 100) / 100, gens

    
    #2 end
    
