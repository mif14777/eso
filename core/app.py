import os
import time
from flask import Flask, render_template, request
from algo import algo
from sunAndWind import getSunAndWindBySubject

app = Flask('core')
app.config['JSON_AS_ASCII'] = False

@app.route('/')
def index():
    return render_template('index.html')

@app.route("/index.html")
def indexHTML():
    return render_template('index.html')

@app.route('/calc')
def calc():
    return render_template('calculator.html')

@app.route('/handle_data', methods=["POST"])
def handle_data():
    sunAndWind = getSunAndWindBySubject(request.form['subject'])
    sunInsolation = float(sunAndWind[0])
    windSpeed = float(sunAndWind[1])
    if request.form['disel-price'] == '':
        diselPrice = 99999999999999999999
    else:
        diselPrice = float(request.form['disel-price'])    
    
    if request.form['methane-price'] == '':
        methanePrice = 99999999999999999999
    else:
        methanePrice = float(request.form['methane-price'])

    energyUsage = float(request.form['energy-usage'])

    if request.form['energy-price'] == '':
        energyPrice = 99999999999999999999
    else:
        energyPrice = float(request.form['energy-price'])
    
    resultPrice, gens = algo(sunInsolation, windSpeed, diselPrice, methanePrice, energyUsage, energyPrice, ['sun', 'wind', 'disel', 'gas'])
    context = {}
    context['data'] = []
    if len(gens) != 0:
        print("gens")
        context['price'] = "Стоимость оборудования: " + str(resultPrice) + " ₽/месяц"
        for i in range(len(gens)):
            name = gens[i][0]
            quantity = gens[i][1]
            context["data"].append([i + 1, name, quantity])
    else:
        print("Common elec")
        context = {"price": "Вам выгодно использовать общую сеть энергоснабжения!"}
    

    time.sleep(0.5)
    return render_template('result.html', context=context)

if __name__ == '__main__':
    app.run(host='192.168.0.2', port=80)